# Neo Personal Website
## polarisfm's ~/ v2

This is a complete remake of my website.

External libraries/tools being used:
- [Hugo](https://gohugo.io), an Apache-2.0 licensed framework for static websites written in Go
- [hello-friend-ng](https://github.com/rhazdon/hugo-theme-hello-friend-ng), an exphat (MIT) licensed theme
