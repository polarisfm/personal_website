---
title: "Projects"
date: 2020-07-09T14:26:15+00:00
tags:
  - Projects
  - Free Software
---
I currently have a few public projects, with more I'm working on privately and hope to publish in the future.

- [My Debian Repository](https://codeberg.org/polarisfm/debian-repo), which supports PureOS/Debian 10
- [uPlexa Unofficial Resources](https://upx.polarisfm.net), a set of unofficial resources for the uPlexa cryptocurrency
