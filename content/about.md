---
title: "About"
date: 2020-07-09T14:23:43+00:00
tags:
  - Introduction
  - Personal
---
I'm Jane. I go by polaris or polarisfm in many places. I'm a lover of Free Software, cola, music. I've switched to PureOS GNU/Linux-libre as my primary distro, though I also run Parabola. I run Replicant on my mobile phone (though it never gets much use).

I use a Librebooted Thinkpad X200. I've installed a wireless card in which requires no proprietary driver, and I've upgraded to 4GB of RAM.

I consider my self a cyberpunk, support copyright abolition, and piracy.
