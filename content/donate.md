---
title: "Donate"
date: 2020-07-18T14:26:15+00:00
tags:
  - Donate
---
If you like what I do, feel free to throw a few bucks my way! This is completely optional, please don't feel obligated.
If you want to donate with complete anonymity, please donate using uPlexa, Monero, or ZCash.

Currency | Address |
--- | --- |
uPlexa | UPX1WVYzszQQzuYd7pYRG95xP2F1d4H2VQT7o5P7XVvJB6rju2RRq4hS8TyqcECAo9CBtyDPrgyB9CUxtdXiAbGa5Bf3rsq2Dr |
Monero | [48RNgWW5AvaR7DjS9sVmBRXFJcoH8nGCceLLR3ehirtmZEKCVxnKrFBb6ArmtSXeDPYJ4ArNkFo7Wjo6K2NwSFJAQc4YyLW](monero:48RNgWW5AvaR7DjS9sVmBRXFJcoH8nGCceLLR3ehirtmZEKCVxnKrFBb6ArmtSXeDPYJ4ArNkFo7Wjo6K2NwSFJAQc4YyLW) |
ZCash | zs1esuk9pwdj9rz692sehe2qqsdx7st3auu3agr40dasfgtdle5rr7ruvk3p5dkc4umvvr5gwasktw |
Bitcoin | [bc1qz3wxspnd9834mfnj5wsg0tga9a80u0k8pa6dpn](bitcoin:bc1qz3wxspnd9834mfnj5wsg0tga9a80u0k8pa6dpn) |
Ethereum | [0x2F9A050C0F040bA4D5fD03696afE57978Cd4d0f0](ethereum:0x2F9A050C0F040bA4D5fD03696afE57978Cd4d0f0) |
Gram (TON) | EQCRhmwfk7G5yCADuc5FnWWax4mEmIDzBoU1hRZzap3W8ocE |
Tron | TGYiap4pU71CUtm9NNtDrJPAMgSayceLLs |
